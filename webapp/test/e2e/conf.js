exports.config = {
    profile: 'integration',
  
    baseUrl: 'http://localhost:8080/index.html',
    
    browsers: [{
        browserName: 'chrome',
            capabilities: {
            chromeOptions: {
                  args: ['--no-sandbox', '--headless', '--disable-gpu', '--disable-setuid-sandbox']
                }
            }
        }
        ]
  };