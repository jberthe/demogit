describe('masterdetail', function () {

    it('should load the app',function() {
      expect(browser.getTitle()).toBe('DemoGitLab');
    });
  
    it('should display the details screen',function() {
      element(by.control({
        viewName: 'demo.gitlab.DemoGitLab.view.View1',
        controlType: 'sap.m.Button',
        properties: {
          text: 'demo1'
        }}))
      .click();
    });
  
    /*it('should validate line items',function() {
      expect(element.all(by.control({
        viewName: 'sap.ui.demo.masterdetail.view.Detail',
        controlType:'sap.m.ColumnListItem'}))
      .count()).toBe(2);
    });*/
  });